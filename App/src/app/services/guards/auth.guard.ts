import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthService) { }

  canActivate() {
    if (localStorage.getItem('token')) {
      return true;
    }

    this.router.navigate(['first-steps']);
    return false;
  }
}
