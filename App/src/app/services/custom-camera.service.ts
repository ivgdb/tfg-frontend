import { Injectable } from '@angular/core';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class CustomCameraService {
  defaultCameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  defaultActionSheetOptions: ActionSheetOptions = {
    buttonLabels: [this.translate.instant('CAMERA.TAKE_PICTURE'), this.translate.instant('CAMERA.SELECT_FROM_GALLERY')],
    addCancelButtonWithLabel: this.translate.instant('CAMERA.CANCEL')
  };

  constructor(private camera: Camera, private actionSheet: ActionSheet, private translate: TranslateService) { }

  public async takePicture(photoUsage: string = 'DEFAULT', allowEdit: boolean = true) {
    const currentActionSheet = Object.assign({}, this.defaultActionSheetOptions);
    currentActionSheet.title = this.translate.instant(`CAMERA.${photoUsage}_CAMERA_ACTION_TITLE`);

    const action = await this.actionSheet.show(currentActionSheet);
    if (this.selectedActionIsInvalid(action)) {
      return { err: this.translate.instant('CAMERA.ERRORS.NO_IMAGE_SELECTED') };
    }

    const currentCameraOptions = Object.assign({}, this.defaultCameraOptions);
    this.updateCurrentCameraOptions(currentCameraOptions, action, allowEdit);

    return this.getPictureFromSelectedSource(currentCameraOptions);
  }

  private updateCurrentCameraOptions(currentCameraOptions: CameraOptions, action: number, allowEdit: boolean) {
    currentCameraOptions.sourceType = action === 1 ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY;
    currentCameraOptions.allowEdit = allowEdit;
  }

  private async getPictureFromSelectedSource(cameraOptions) {
    let imageData;

    try {
      imageData = await this.camera.getPicture(cameraOptions);
    } catch (err) {
      return { err: this.translate.instant('CAMERA.NO_IMAGE_SELECTED') };
    }

    return imageData;
  }

  private selectedActionIsInvalid(action: number) {
    return !action || action >= 3;
  }
}
