import { Injectable } from "@angular/core";
import { from, forkJoin, of, Observable, throwError } from "rxjs";
import {
  catchError,
  mapTo,
  delayWhen,
  switchMap,
  shareReplay,
  startWith
} from "rxjs/operators";
import {
  AlertController,
  LoadingController,
  ToastController
} from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root"
})
export class OverlaysService {
  constructor(
    private loadingController: LoadingController,
    private alertController: AlertController,
    private toastController: ToastController,
    private translate: TranslateService
  ) {}

  presentConfirm(message: string) {
    return Observable.create(async observer => {
      const alert = await this.alertController.create({
        message: this.translate.instant(message),
        buttons: [
          {
            text: this.translate.instant("ACTIONS.CANCEL"),
            role: "cancel",
            cssClass: "secondary",
            handler: () => observer.error("error")
          },
          {
            text: this.translate.instant("ACTIONS.CONFIRM"),
            handler: () => {
              observer.next(true);
              observer.complete();
            }
          }
        ]
      });
      alert.present();
    });
  }
  requestWithLoaderAndError(request: () => Observable<any>, message?) {
    let obs = of({}).pipe(
      this.presentLoader(),
      switchMap(() =>
        request().pipe(this.onErrorDismissLoaderAndPresentError())
      ),
      this.dismissLoader(),
      shareReplay()
    );

    if (message) {
      obs = obs.pipe(this.presentToast(message));
    }

    obs.subscribe(() => {});
    return obs;
  }

  buildLoader(obs: Observable<any>) {
    return obs.pipe(
      mapTo(false),
      startWith(true),
      shareReplay()
    );
  }

  presentLoader() {
    return delayWhen(() => from(this.presentIonicLoader()));
  }

  dismissLoader() {
    return delayWhen(() => from(this.dismissIonicLoader()));
  }

  onErrorDismissLoaderAndPresentError() {
    return catchError(error =>
      forkJoin(
        from(this.dismissIonicLoader()),
        from(this.presentError(error.message))
      ).pipe(switchMap(() => throwError({ error: error.array })))
    );
  }

  async presentError(error) {
    const alert = await this.alertController.create({
      message: error,
      buttons: ["OK"]
    });
    await alert.present();
  }

  presentToast(message) {
    return delayWhen(() => from(this.presentIonicToast(message)));
  }

  private async presentIonicLoader() {
    if (!(await this.loadingController.getTop())) {
      const loading = await this.loadingController.create();
      await loading.present();
    }
  }

  private async dismissIonicLoader() {
    if (await this.loadingController.getTop()) {
      await this.loadingController.dismiss();
    }
  }

  private async presentIonicToast(message) {
    const toast = await this.toastController.create({
      message: this.translate.instant(message),
      duration: 5000
    });
    await toast.present();
  }
}
