import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { AuthService } from "../auth.service";
import { OverlaysService } from '../overlays.service';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private overlaysService: OverlaysService) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        const token = localStorage.getItem("token");
        if (err.status === 401 && token) {
          this.overlaysService.presentError("Session expired").then(
            () => {
              this.authService.logout();
            }
          );
        }

        if (err.status === 404) {
          alert("This endpoint does not exists: " + request.url);
        }
        const errObj = err.error;
        const error = errObj.message || err.error;
        return throwError({
          status: err.status,
          message: error,
          array: errObj
        });
      })
    );
  }
}
