import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { pluck, shareReplay, tap, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../entities/user.model';

interface State {
  user?: User;
  token?: string;
  isLogged: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  initialState: State = {
    isLogged: false
  };

  private loggedUserSubject$ = new BehaviorSubject<State>(this.initialState);
  loggedUser$ = this.loggedUserSubject$.asObservable();

  constructor(private http: HttpClient, private router: Router) {
    const token = localStorage.getItem('token');
    if (token) {
      const user = JSON.parse(localStorage.getItem('user'));
      this.loggedUserSubject$.next({ user: user ? new User(user) : null, isLogged: true });
    }
  }

  get state() {
    return this.loggedUserSubject$.getValue();
  }

  get currentUser() {
    return this.loggedUserSubject$.getValue().user;
  }

  login({ username, password }) {
    const request = this.http.post('/login', { username, password }).pipe(
      shareReplay(),
      pluck<object, string>('token'),
      tap(token => localStorage.setItem('token', token)),
      switchMap(() => this.getCurrentUser())
    );

    return request;
  }

  impersonateLogin() { }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.loggedUserSubject$.next(this.initialState);
    this.router.navigate(['login']);
  }

  register(newUser) {
    return this.http.post('/users/register', newUser);
  }

  confirmRegister(token) {
    return this.http.post('/auth/register/confirm', { token });
  }

  resetPassword(email) {
    return this.http.post('/auth/reset-password', { email });
  }

  changePassword(token, { password, repassword }) {
    return this.http.post('/auth/change-password', { token, password, repassword });
  }

  getCurrentUser() {
    this.loggedUserSubject$.next({ ...this.state });

    const request = this.http.get<User>('/users/current').pipe(shareReplay());
    request.subscribe(
      user => {
        localStorage.setItem('user', JSON.stringify(user));
        this.loggedUserSubject$.next({ user: new User(user), isLogged: true });
      },
      error => {
        localStorage.removeItem('user');
        this.loggedUserSubject$.next({ ...this.state });
      }
    );

    return request;
  }

  saveCurrentUser(user) {
    this.loggedUserSubject$.next({ ...this.state });

    const request = this.http.post<User>(`/users/${user.uuid}`, user).pipe(shareReplay());
    request.subscribe(
      currentUser => {
        localStorage.setItem('user', JSON.stringify(currentUser));
        this.loggedUserSubject$.next({ user: new User(currentUser), isLogged: true });
      },
      error => this.loggedUserSubject$.next({ ...this.state })
    );

    return request;
  }
}
