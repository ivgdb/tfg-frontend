import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/services/guards/auth.guard';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{ path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard] },
	{ path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule', canActivate: [AuthGuard] },
	{ path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard] },
	{ path: 'search', loadChildren: './pages/search/search.module#SearchPageModule', canActivate: [AuthGuard] },
	{ path: 'games/:uuid', loadChildren: './pages/game/game.module#GamePageModule', canActivate: [AuthGuard] },
	{ path: 'profile-edit', loadChildren: './pages/profile/profile-edit/profile-edit.module#ProfileEditPageModule', canActivate: [AuthGuard] },
	{ path: 'my-reviews', loadChildren: './pages/profile/my-reviews/my-reviews.module#MyReviewsPageModule', canActivate: [AuthGuard] },
	{ path: 'my-comments', loadChildren: './pages/profile/my-comments/my-comments.module#MyCommentsPageModule', canActivate: [AuthGuard] },
	{ path: 'custom-lists', loadChildren: './pages/profile/custom-lists/custom-lists.module#CustomListsPageModule', canActivate: [AuthGuard] },
	{ path: 'first-steps', loadChildren: './pages/first-steps/first-steps.module#FirstStepsPageModule' },
	{ path: 'login', loadChildren: './pages/first-steps/login/login.module#LoginPageModule' },
	{ path: 'sign-up', loadChildren: './pages/first-steps/sign-up/sign-up.module#SignUpPageModule' },  { path: 'create-playlist', loadChildren: './pages/create-playlist/create-playlist.module#CreatePlaylistPageModule' },
  { path: 'legal-doc', loadChildren: './pages/profile/legal-doc/legal-doc.module#LegalDocPageModule' }


];
@NgModule({
	imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
