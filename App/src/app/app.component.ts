import { Component, ViewChild } from '@angular/core';

import { Platform, IonMenu } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Http, Response } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  @ViewChild(IonMenu) menu: IonMenu;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private authService: AuthService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.translate.setDefaultLang(environment.INTERFACE.APP_DEFAULT_LANGUAGE);

    Promise.all([
      this.platform.ready(),
      this.translate.use(environment.INTERFACE.APP_DEFAULT_LANGUAGE).toPromise()
    ]).then(() => {
      if (!this.platform.is('cordova')) {
        return;
      }

      this.splashScreen.hide();

    });
  }

  /*initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }*/

  closeMenu() {
    this.menu.close();
  }

  logout() {
    this.closeMenu();
    this.authService.logout();
  }
}
