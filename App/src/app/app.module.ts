import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router'; 0

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Camera } from '@ionic-native/camera/ngx';
import { ActionSheet } from '@ionic-native/action-sheet/ngx';
import { RequestInterceptor } from './services/interceptors/request.interceptor';
import { TokenInterceptor } from './services/interceptors/token.interceptor';
import { GameFranchiseComponent } from './components/game-franchise/game-franchise.component';
import { ErrorsInterceptor } from './services/interceptors/errors.interceptor';
import { VoteActionSheetComponent } from './components/vote-action-sheet/vote-action-sheet.component';
import { UserPlaylistComponent } from './components/user-playlist/user-playlist.component';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
	declarations: [AppComponent],
	entryComponents: [GameFranchiseComponent, VoteActionSheetComponent, UserPlaylistComponent],
	imports: [
		BrowserModule,
		IonicModule.forRoot(),
		AppRoutingModule,
		ComponentsModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
	],
	providers: [
		StatusBar,
		SplashScreen,
		Camera,
		ActionSheet,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		{ provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptor, multi: true }
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
