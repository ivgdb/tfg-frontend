import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { OverlaysService } from "src/app/services/overlays.service";
import { AuthService } from "src/app/services/auth.service";
import { Router } from "@angular/router";
import { filter, tap, pluck } from "rxjs/operators";
import { UserService } from 'src/app/entities/user.service';

@Component({
  selector: "app-profile-edit",
  templateUrl: "./profile-edit.page.html",
  styleUrls: ["./profile-edit.page.scss"]
})
export class ProfileEditPage {
  profileForm: FormGroup = this.fb.group(
    {
      name: [""],
      email: [""],
      nickname: [""],
      password: [""],
      passwordRepeat: [""],
      avatar: [""]
    }
  );

  constructor(
    private fb: FormBuilder,
    private overlaysService: OverlaysService,
    private authService: AuthService,
    private router: Router,
    private userService: UserService
  ) { }

  // This functions is done in the back
  //
  // private validatePasswords(group: FormGroup) {
  //   const pass1 = group.controls.password.value;
  //   const pass2 = group.controls.passwordRepeat.value;
  //   return pass1 === pass2 ? null : { notSame: true };
  // }

  ionViewWillEnter() {
    this.authService.getCurrentUser().subscribe((user) => {
      this.profileForm.patchValue(user);
    });

  }

  submit() {
    if (this.profileForm.valid) {
      this.overlaysService
        .requestWithLoaderAndError(() => this.userService.updateUser(this.profileForm.value), "Profile updated successfully")
        .subscribe(
          () => {
            this.router.navigate(["/tabs/profile"]);
          }
        );
    } else {
      this.overlaysService.presentError("Couldn't do request");
    }
  }
}
