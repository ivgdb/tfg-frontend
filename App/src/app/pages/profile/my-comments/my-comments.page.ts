import { Component, OnInit } from '@angular/core';
import { CommentsService } from 'src/app/entities/comments';

@Component({
	selector: 'app-my-comments',
	templateUrl: './my-comments.page.html',
	styleUrls: [ './my-comments.page.scss' ]
})
export class MyCommentsPage implements OnInit {
	comments$;
	constructor(private commentsService: CommentsService) {}

	expand = false;

	toggleExpand() {
		this.expand = !this.expand;
	}

	ngOnInit() {
		this.comments$ = this.commentsService.getByUser();
	}
}
