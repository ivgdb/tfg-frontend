import { Component, OnInit } from '@angular/core';
import { PopoverController, ModalController } from '@ionic/angular';
import { FiltersPopoverComponent } from 'src/app/components/filters-popover/filters-popover.component';
import { RatingsService } from 'src/app/entities/ratings';

@Component({
  selector: 'app-my-reviews',
  templateUrl: './my-reviews.page.html',
  styleUrls: ['./my-reviews.page.scss'],
})
export class MyReviewsPage {
  ratings$;
  filterLabel = 'Filters';
  constructor(private ratingsService: RatingsService, private popoverController: PopoverController) {
    this.ratings$ = this.ratingsService.getUserRatings('noFilter');
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: FiltersPopoverComponent,
      event: ev,
      mode: "md",
      cssClass: 'contact-popover'
    });

    await popover.present();

    popover.onDidDismiss().then((modalData: any) => {
      if (modalData.data) {
        const filter = modalData.data.selectedData;
        this.ratings$ = this.ratingsService.getUserRatings(filter);
        this.filterLabel = 'Filter by '+filter;
      }

    });
  }


}
