import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyReviewsPage } from './my-reviews.page';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
	{
		path: '',
		component: MyReviewsPage
	}
];

@NgModule({
	imports: [ CommonModule, ComponentsModule, FormsModule, IonicModule, RouterModule.forChild(routes) ],
	declarations: [ MyReviewsPage ]
})
export class MyReviewsPageModule {}
