import { Component, OnInit, ViewChild } from '@angular/core';
import { IonReorderGroup, ModalController } from '@ionic/angular';
import { GamelistsService } from 'src/app/entities/gamelists';
import { map } from 'rxjs/operators';
import { UserPlaylistComponent } from 'src/app/components/user-playlist/user-playlist.component';

@Component({
	selector: 'app-custom-lists',
	templateUrl: './custom-lists.page.html',
	styleUrls: ['./custom-lists.page.scss']
})
export class CustomListsPage {
	@ViewChild(IonReorderGroup) reorderGroup: IonReorderGroup;
	customLists$;
	gameLists$;
	customLists = [];

	constructor(private modals: ModalController, private gamelistsService: GamelistsService) {
		this.gameLists$ = this.gamelistsService.getByUser();
		this.customLists$ = this.gamelistsService.selectAll().pipe(
			map((game: any) => {
				return game.filter(game => game.name !== 'Favorite Games' && game.name !== 'Wish List');
			})
		);	
		this.customLists$.subscribe(list => {
			console.log(list);
			list.forEach(element => {
				this.customLists.push(element);
			});
		})
	}

	ionViewWillEnter() {
		this.customLists = [];
	}

	doReorder(ev: any) {
		this.customLists = ev.detail.complete(this.customLists);
	}

	toggleReorderGroup() {
		this.reorderGroup.disabled = !this.reorderGroup.disabled;
	}

	async openPlaylistModal(gameList) {
		const playlistModalConfig = {
			gameList: gameList
		};

		const modal = await this.modals.create({
			component: UserPlaylistComponent,
			cssClass: 'pp-modal',
			componentProps: playlistModalConfig
		});

		await modal.present();
	}
}
