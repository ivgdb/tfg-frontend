import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ModalController } from '@ionic/angular';
import { UserPlaylistComponent } from 'src/app/components/user-playlist/user-playlist.component';
import { GamelistsService } from 'src/app/entities/gamelists';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {

  user$;
  gameList$;
  user;

  constructor(private modals: ModalController, private authService: AuthService, private gamelistsService: GamelistsService) {
   }

  ionViewWillEnter() {
    this.user$ = this.authService.getCurrentUser();
    this.gameList$ = this.gamelistsService.getByUser();
    this.gameList$ = this.gamelistsService.selectAll().pipe(
      map((game: any) => {
        return game.filter(game => game.name === 'Favorite Games');
      })
    );
  }

  logout() {
    this.authService.logout();
  }

  async openPlaylistModal(gameLists, listName) {
    const playlistModalConfig = {
      gameLists: gameLists,
      name: listName
    };

    const modal = await this.modals.create({
      component: UserPlaylistComponent,
      cssClass: 'pp-modal',
      componentProps: playlistModalConfig
    });

    await modal.present();
  }

}
