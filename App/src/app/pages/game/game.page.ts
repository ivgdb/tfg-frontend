import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OverlaysService } from 'src/app/services/overlays.service';
import { pluck, map } from 'rxjs/operators';
import { VideogamesService } from 'src/app/entities/videogames.service';
import { ModalController, PopoverController } from '@ionic/angular';
import { VoteActionSheetComponent } from 'src/app/components/vote-action-sheet/vote-action-sheet.component';
import { RatingsService } from 'src/app/entities/ratings';
import { GamelistsService } from 'src/app/entities/gamelists';
import { DomSanitizer } from '@angular/platform-browser';
import { OptionsPopoverComponent } from 'src/app/components/options-popover/options-popover.component';
import { CommentsService } from 'src/app/entities/comments';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage {
  isLoading$;
  videogame$;
  fullView = false;
  userRating$;
  gameList$;
  gameLists = [];
  favoritePlaylist;
  user;
  videogame = [];
  gameUuid: string;
  isInPlaylist;
  added = false;
  comment;
  comments$;
  websites$;
  websites = [];

  constructor(private commentsService: CommentsService, private popoverController: PopoverController, private gamelistsService: GamelistsService, private ratingsService: RatingsService, private modals: ModalController, private route: ActivatedRoute, private overlaysService: OverlaysService, private videogamesService: VideogamesService, private sanitizer: DomSanitizer) { }

  ionViewWillEnter() {
    this.route.params.pipe(pluck('uuid')).subscribe(async (gameUuid: string) => {
      this.gameUuid = gameUuid.toString();
      this.isLoading$ = this.overlaysService.buildLoader(this.videogamesService.getByUuid(gameUuid));
      this.videogame$ = this.videogamesService.getByUuid(gameUuid);
      this.comments$ = this.commentsService.getByVideogame(gameUuid);
      
      this.videogame$.pipe(pluck('websites')).subscribe((websites:any) => {
        websites.filter(website => {
          if(website.category === 15 || website.category === 1 || website.category === 5 || website.category === 4 || website.category === 9){
            this.websites.push(website);
          }
        })
      });

    });

    this.userRating$ = this.ratingsService.getVideogameRatingByUser(this.gameUuid);

    this.gameList$ = this.gamelistsService.getByUser();
    this.gameList$.subscribe((list: any) => {
      list.filter(list => {
        if(list.name === 'Favorite Games') {
          this.favoritePlaylist = list;
        }
      });
      // this.gamelistsService.checkGamePlaylist(this.gameUuid+'', this.favoritePlaylist.uuid).subscribe(
      //   (data: any) => { console.log(data) }
      // );
  
    });

    
    // this.gameList$.subscribe((list) => {
    //   if (list.length > 0) {
    //     this.gameLists.push(list[0]);
    //     this.favoritePlaylist = this.gameLists[0];
    //     this.gamelistsService.checkGamePlaylist(this.gameUuid, this.favoritePlaylist.uuid).subscribe(
    //       (data: any) => { console.log(data) }
    //     );
    //   }

    // });

  }

  addToFavorites(uuid) {
    this.overlaysService.requestWithLoaderAndError(
      () => this.gamelistsService.addGameToPlaylist(uuid, this.favoritePlaylist.uuid)
    ).subscribe(
      () => this.added = true
    );
  }

  addComment() {
    this.overlaysService
      .requestWithLoaderAndError(() => this.commentsService.addComment(this.comment, this.gameUuid))
      .subscribe(
        () => {
          this.comments$ = this.commentsService.getByVideogame(this.gameUuid);
          this.comment = "";
        }
      );
  }

  async openVotingModal(game) {
    const votingModalConfig = {
      game: game
    };

    const modal = await this.modals.create({
      component: VoteActionSheetComponent,
      cssClass: 'voting-modal',
      componentProps: votingModalConfig,
      showBackdrop: true,
      backdropDismiss: true,
      keyboardClose: true
    });

    await modal.present();

    modal.onDidDismiss().then((modalData: any) => {
      if (modalData.data) {
        const rating = modalData.data.selectedData;

        this.overlaysService.requestWithLoaderAndError(() =>
          this.ratingsService.addRating(game, rating)
        ).subscribe(() =>
          this.userRating$ = this.ratingsService.getVideogameRatingByUser(this.gameUuid)
        );
      }

    });
  }

  async openOptionsPopover(gameUuid) {
    const optionsModalConfig = {
      gameUuid: gameUuid
    };

    const popover = await this.popoverController.create({
      component: OptionsPopoverComponent,
      componentProps: optionsModalConfig,
      mode: "md",
      cssClass: 'options-popover'
    });
    return await popover.present();
  }

  trustURL(url) {
    let newURL;
    newURL = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    return newURL;
  }
}