import { Component, OnInit } from '@angular/core';
import { OverlaysService } from 'src/app/services/overlays.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private overlaysService: OverlaysService,
    private authService: AuthService
  ) { }


  signForm: FormGroup = this.fb.group(
    {
      email: ["", Validators.required],
      name: ["", Validators.required],
      nickname: ["", Validators.required],
      password: ["", Validators.required],
      passwordRepeat: ["", Validators.required],
    },
    { validators: this.validatePasswords }
  );

  private validatePasswords(group: FormGroup) {
    const pass1 = group.controls.password.value;
    const pass2 = group.controls.passwordRepeat.value;
    return pass1 === pass2 ? null : { notSame: true };
  }

  signUp() {
    if (this.signForm.valid) {
      this.overlaysService
        .requestWithLoaderAndError(() =>
          this.authService.register(this.signForm.value), "Account created successfully"
        )
        .subscribe(() => {
          this.router.navigate(["/login"]);
        });
    } else {
      this.overlaysService.presentError("Fill in all the fields");
    }
  }

}
