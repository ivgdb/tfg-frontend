import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { OverlaysService } from 'src/app/services/overlays.service';
import { AuthService } from 'src/app/services/auth.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  subscription;

  constructor(
    private fb: FormBuilder,
    private overlaysService: OverlaysService,
    private authService: AuthService,
    private router: Router,
    private platform: Platform
  ) { }


  loginForm: FormGroup = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  login() {
    if (this.loginForm.valid) {
      this.overlaysService
        .requestWithLoaderAndError(() =>
          this.authService.login(this.loginForm.value)
        )
        .subscribe(() => {
          this.router.navigate(["/tabs/home"]);
        });
    } else {
      this.overlaysService.presentError("Fill in all the fields");
    }
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      this.router.navigate(["/first-steps"]);
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

}
