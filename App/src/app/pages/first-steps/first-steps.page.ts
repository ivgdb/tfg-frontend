import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-first-steps',
	templateUrl: './first-steps.page.html',
	styleUrls: [ './first-steps.page.scss' ]
})
export class FirstStepsPage implements OnInit {
	sliderOptions = {
		zoom: {
			toggle: false
		},
		pagination: {
			el: '.swiper-pagination'
		}
	};

	constructor() {}

	ngOnInit() {}
}
