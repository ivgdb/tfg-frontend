import { Component, OnInit } from '@angular/core';
import { VideogamesService } from 'src/app/entities/videogames.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.page.html',
	styleUrls: ['./home.page.scss']
})
export class HomePage {
	newsSlidersOptions = {
		slidesPerView: 1.25,
		slidesOffsetBefore: 16,
		slidesOffsetAfter: 16,
		centerInsufficientSlides: true,
		spaceBetween: 8,
		zoom: false
	};

	gamesSlidersOptions = {
		slidesPerView: 2.25,
		slidesOffsetBefore: 80,
		slidesOffsetAfter: 102,
		centerInsufficientSlides: true,
		spaceBetween: 150,
		zoom: false
	};

	latestGames$;
	news = [1, 2, 3, 4, 5, 6];

	constructor(private gameService: VideogamesService) { }

	ionViewWillEnter() {
		this.latestGames$ = this.gameService.getLatestGames();
	}

}
