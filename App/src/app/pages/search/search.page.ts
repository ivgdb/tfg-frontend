import { Component, OnInit } from '@angular/core';
import { OverlaysService } from 'src/app/services/overlays.service';
import { VideogamesService } from 'src/app/entities/videogames.service';
import { GameFranchiseComponent } from 'src/app/components/game-franchise/game-franchise.component';
import { ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage{
  videogames$;
  isLoading$;
  gameList;
  latest$;
  searchQuery;
  searchTerm$ = new Subject<string>();


  constructor(private modals: ModalController, private overlaysService: OverlaysService, private videogamesService: VideogamesService) { }

  ionViewWillEnter(){
    this.isLoading$ = this.overlaysService.buildLoader(
      this.videogames$ = this.videogamesService.getAll()
    );
    this.videogames$.subscribe( videogames => this.gameList = videogames);
  }

  findGame() {
    if (this.searchQuery === '') {
      return this.videogames$ = this.videogamesService.getAll();
    }
    this.videogames$ = this.videogamesService.getByName(this.searchQuery);
  }

  async openFranchiseModal(franchise, game) {
    const franchiseModalConfig = {
      franchise: franchise,
      gameUuid: game.uuid,
      gameList: this.gameList
    };

    const modal = await this.modals.create({
      component: GameFranchiseComponent,
      cssClass: 'pp-modal',
      componentProps: franchiseModalConfig
    });
    await modal.present();
  }
}
