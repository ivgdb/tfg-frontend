import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GamelistsService } from 'src/app/entities/gamelists';
import { OverlaysService } from 'src/app/services/overlays.service';

@Component({
  selector: 'app-create-playlist',
  templateUrl: './create-playlist.page.html',
  styleUrls: ['./create-playlist.page.scss'],
})
export class CreatePlaylistPage implements OnInit {
  gameUuid;
  name;
  constructor(private modals: ModalController, private gamelistsService: GamelistsService, private overlaysService: OverlaysService) { }

  ngOnInit() {
  }

  createPlaylist() {
    this.dismiss();
    this.overlaysService.requestWithLoaderAndError(
      () => this.gamelistsService.createPlaylist(this.name, this.gameUuid), "Playlist created and game added"
    ).subscribe(
      () => this.dismiss
    );
  }

  dismiss() {
    this.modals.dismiss();
  }
}