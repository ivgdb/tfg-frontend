import { Component, OnInit, Input } from '@angular/core';
import { RatingsService } from 'src/app/entities/ratings';

@Component({
  selector: 'app-game-review-item',
  templateUrl: './game-review-item.component.html',
  styleUrls: ['./game-review-item.component.scss'],
})
export class GameReviewItemComponent{
  @Input() rating: any; 
  rating$;
  constructor(private ratingsService: RatingsService) {
   }

}
