import { Component, OnInit, Input } from '@angular/core';
import { text } from '@angular/core/src/render3';

@Component({
	selector: 'app-game-comment-item',
	templateUrl: './game-comment-item.component.html',
	styleUrls: [ './game-comment-item.component.scss' ]
})
export class GameCommentItemComponent implements OnInit {
	@Input() textExpand;
	@Input() comment;
	constructor() {}

	ngOnInit() {}
}
