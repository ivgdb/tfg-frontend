import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-game-small-card',
  templateUrl: './game-small-card.component.html',
  styleUrls: ['./game-small-card.component.scss'],
})
export class GameSmallCardComponent {
  @Input()
  game;

  constructor() { }

}
