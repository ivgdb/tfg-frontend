import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
@Component({
	selector: 'app-filters-popover',
	templateUrl: './filters-popover.component.html',
	styleUrls: ['./filters-popover.component.scss']
})
export class FiltersPopoverComponent {
	selectedData: any;
	filter;
	constructor(private popover: PopoverController) { }

	filterList = false;


	toggleList() {
		this.filterList = !this.filterList;
	}

	filterRatings() {
		this.selectedData = this.filter;
		this.popover.dismiss({ selectedData: this.selectedData });
	}
}
