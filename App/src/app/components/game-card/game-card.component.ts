import { Component, OnInit, Input } from '@angular/core';
import { Videogame } from 'src/app/entities/videogames.model';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss'],
})
export class GameCardComponent{
  @Input() game: any;
  constructor() { }

}
