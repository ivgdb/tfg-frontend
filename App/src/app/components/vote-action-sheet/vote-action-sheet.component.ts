import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-vote-action-sheet',
  templateUrl: './vote-action-sheet.component.html',
  styleUrls: ['./vote-action-sheet.component.scss'],
})
export class VoteActionSheetComponent {
  game;
  rating;
  selectedData;
  constructor(private modals: ModalController) { }


  sendRating() {
    this.selectedData = this.rating;
    this.dismiss();
  }

  closeModal() {
    this.modals.dismiss();
  }

  dismiss() {
    this.modals.dismiss({ selectedData: this.selectedData });
  }
}
