import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-game-comments',
  templateUrl: './game-comments.component.html',
  styleUrls: ['./game-comments.component.scss'],
})
export class GameCommentsComponent {
  @Input() comment;
  constructor() {
  }
}
