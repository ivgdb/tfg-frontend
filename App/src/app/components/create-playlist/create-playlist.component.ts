import { Component, OnInit } from '@angular/core';
import { VideogamesService } from 'src/app/entities/videogames.service';
import { ModalController } from '@ionic/angular';
import { GamelistsService } from 'src/app/entities/gamelists';
import { OverlaysService } from 'src/app/services/overlays.service';

@Component({
  selector: 'app-create-playlist',
  templateUrl: './create-playlist.component.html',
  styleUrls: ['./create-playlist.component.scss'],
})
export class CreatePlaylistComponent {
  gameUuid;
  name;
  constructor(private modals: ModalController, private gamelistsService: GamelistsService, private overlaysService: OverlaysService) { }

  createPlaylist() {
    this.dismiss();
    this.overlaysService.requestWithLoaderAndError(
      () => this.gamelistsService.createPlaylist(this.name, this.gameUuid), "Playlist created and game added"
    ).subscribe(
      () => this.dismiss
    );
  }

  dismiss() {
    this.modals.dismiss();
  }
}
