import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { GameCardComponent } from './game-card/game-card.component';
import { ProfileCardComponent } from './profile-card/profile-card.component';
import { GameReviewItemComponent } from './game-review-item/game-review-item.component';
import { FiltersPopoverComponent } from './filters-popover/filters-popover.component';
import { GameCommentItemComponent } from './game-comment-item/game-comment-item.component';
import { NewsCardComponent } from './news-card/news-card.component';
import { GameSmallCardComponent } from './game-small-card/game-small-card.component';
import { ImagePickerComponent } from './image-picker/image-picker.component';
import { GameFranchiseComponent } from './game-franchise/game-franchise.component';
import { VoteActionSheetComponent } from './vote-action-sheet/vote-action-sheet.component';
import { UserPlaylistComponent } from './user-playlist/user-playlist.component';
import { FormsModule } from '@angular/forms';
import { OptionsPopoverComponent } from './options-popover/options-popover.component';
import { CreatePlaylistComponent } from './create-playlist/create-playlist.component';
import { GameCommentsComponent } from './game-comments/game-comments.component';
import { TimeagoModule } from "ngx-timeago";

@NgModule({
	declarations: [
		GameCommentsComponent,
		GameFranchiseComponent,
		VoteActionSheetComponent,
		UserPlaylistComponent,
		GameCardComponent,
		ProfileCardComponent,
		GameReviewItemComponent,
		OptionsPopoverComponent,
		CreatePlaylistComponent,
		FiltersPopoverComponent,
		GameCommentItemComponent,
		NewsCardComponent,
		GameSmallCardComponent,
		ImagePickerComponent
	],
	imports: [CommonModule, FormsModule, IonicModule, RouterModule, TimeagoModule.forRoot()],
	exports: [
		CommonModule,
		GameCardComponent,
		ProfileCardComponent,
		GameReviewItemComponent,
		GameCommentItemComponent,
		FiltersPopoverComponent,
		OptionsPopoverComponent,
		CreatePlaylistComponent,
		NewsCardComponent,
		GameSmallCardComponent,
		ImagePickerComponent,
		GameFranchiseComponent,
		VoteActionSheetComponent,
		UserPlaylistComponent,
		GameCommentsComponent,
		TimeagoModule
	],
	entryComponents: [GameCardComponent, FiltersPopoverComponent, CreatePlaylistComponent, OptionsPopoverComponent, GameCommentsComponent]
})
export class ComponentsModule { }
