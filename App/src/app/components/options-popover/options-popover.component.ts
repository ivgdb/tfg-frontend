import { Component, OnInit } from '@angular/core';
import { GamelistsService } from 'src/app/entities/gamelists';
import { ModalController, PopoverController } from '@ionic/angular';
import { VoteActionSheetComponent } from '../vote-action-sheet/vote-action-sheet.component';
import { CreatePlaylistComponent } from '../create-playlist/create-playlist.component';
import { OverlaysService } from 'src/app/services/overlays.service';

@Component({
  selector: 'app-options-popover',
  templateUrl: './options-popover.component.html',
  styleUrls: ['./options-popover.component.scss'],
})
export class OptionsPopoverComponent implements OnInit {
  gamelists$;
  gameUuid;
  constructor(private modals: ModalController, private popoverController: PopoverController, private gamelistsService: GamelistsService, private overlaysService: OverlaysService) { }

  ngOnInit() {
    this.gamelists$ = this.gamelistsService.getByUser();
    
  }
  async openCreatePlaylistModal(gameUuid) {
    const playlistModalConfig = {
      gameUuid: gameUuid
    };

    const modal = await this.modals.create({
      component: CreatePlaylistComponent,
      cssClass: 'voting-modal',
      componentProps: playlistModalConfig,
      showBackdrop: true,
      backdropDismiss: true
    });

    await modal.present();
  }

  addGameToPlaylist(uuid) {
    this.overlaysService.requestWithLoaderAndError(
      () => this.gamelistsService.addGameToPlaylist(this.gameUuid, uuid), "Game Added"
    );
  }

  dismiss() {
    this.popoverController.dismiss();
    this.openCreatePlaylistModal(this.gameUuid);
  }
}
