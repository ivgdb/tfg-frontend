import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { VideogamesService } from 'src/app/entities/videogames.service';
import { GamelistsService } from 'src/app/entities/gamelists';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-user-playlist',
  templateUrl: './user-playlist.component.html',
  styleUrls: ['./user-playlist.component.scss'],
})
export class UserPlaylistComponent {
  gameList$;
  gameLists = [];
  name;
  gameList;
  constructor(private modals: ModalController, private gamelistsService: GamelistsService) {

  }

  ionViewWillEnter() {
    switch (this.name) {
      case 'Favorite':
        this.gameList$ = this.gamelistsService.getByUser();
        this.gameList$ = this.gamelistsService.selectAll().pipe(
          map((game: any) => {
            return game.filter(game => game.name === 'Favorite Games');
          })
        );
        this.getGamelist();
        break;
      case 'Wishlist':
        this.gameList$ = this.gamelistsService.getByUser();
        this.gameList$ = this.gamelistsService.selectAll().pipe(
          map((game: any) => {
            return game.filter(game => game.name === 'Wish List');
          })
        );
        this.getGamelist();
        break;
    }

  }

  private getGamelist() {
    this.gameList$.subscribe(list => {
      if (list.length > 0) {
        this.gameLists = [];
        this.gameLists.push(list[0]);
        this.gameList = this.gameLists[0];
      }
    });
  }

  dismiss() {
    this.modals.dismiss();
  }
}
