import { Component, Output, EventEmitter } from "@angular/core";

import { CustomCameraService } from "../../services/custom-camera.service";
import { OverlaysService } from "../../services/overlays.service";

@Component({
  selector: "app-image-picker",
  templateUrl: "./image-picker.component.html",
  styleUrls: ["./image-picker.component.scss"]
})
export class ImagePickerComponent {
  @Output() pictureSelected: EventEmitter<any> = new EventEmitter();

  constructor(
    private camera: CustomCameraService,
    private overlaysService: OverlaysService
  ) {}

  async takePicture() {
    const img: any = await this.camera.takePicture("PROFILE");

    if (img.err) {
      this.overlaysService.presentToast("CAMERA.ERRORS.NO_IMAGE_SELECTED");
      return;
    }

    this.pictureSelected.emit({ img });
  }
}
