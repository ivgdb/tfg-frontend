import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VideogamesService } from 'src/app/entities/videogames.service';
import { map, filter } from 'rxjs/operators';
import { Videogame } from 'src/app/entities/videogames.model';

@Component({
  selector: 'app-game-franchise',
  templateUrl: './game-franchise.component.html',
  styleUrls: ['./game-franchise.component.scss'],
})
export class GameFranchiseComponent {
  franchise;
  gameUuid;
  gameList$;
  franchiseList$;
  franchiseList = [];
  slideOpts;
  isLoading$;
  index;

  constructor(private modals: ModalController, private videogamesService: VideogamesService) {
  }

  ionViewWillEnter() {
    this.franchiseList$ = this.videogamesService.getAll().pipe(
      map((videogames: any) => {
        return videogames.filter(videogame => videogame.franchise === this.franchise);
      })
    );

    let index = 0;

    this.franchiseList$.subscribe((videogames: any) => {
      videogames.forEach(game => {
        this.franchiseList.push(game);
        if (game.uuid === this.gameUuid) {
          this.slideOpts = {
            initialSlide: index,
            speed: 400
          };
        }
        index++;
      });
    });

  };

  dismiss() {
    this.modals.dismiss();
  }

}
