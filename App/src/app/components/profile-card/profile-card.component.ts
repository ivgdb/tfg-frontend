import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/entities/user.service';
import { OverlaysService } from 'src/app/services/overlays.service';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss'],
})
export class ProfileCardComponent {
  @Input() userData;
  @Input() gameList;

  gameList$;
  profileImage = "http://sg-fs.com/wp-content/uploads/2017/08/user-placeholder.png";

  constructor(private userService: UserService, private overlaysService: OverlaysService) { }

  changeImage($event) {
    this.profileImage = 'data:image/jpeg;base64,' + $event.img;
    this.userData.avatar = this.profileImage;
    this.overlaysService
      .requestWithLoaderAndError(() => this.userService.updateUser(this.userData), 'Image updated')
      .subscribe(() => { });
  }

}
