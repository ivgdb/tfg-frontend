import { BaseEntityService, EntityAPI, State } from './base-entity.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { pluck, map, filter } from 'rxjs/operators';

import { Entity } from './base-entity.model';
import { pipe } from '@angular/core/src/render3';
import { forkJoin } from 'rxjs';
import { AuthService } from '../services/auth.service';

export class Videogame extends Entity {
  franchise: string;
  cover: string;

  constructor(videogame) {
    super(videogame);
  }
}
