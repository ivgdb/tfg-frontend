export abstract class Entity {
  id: number;

  static findById(entities, id) {
    return entities.find(entity => entity.id == id);
  }

  constructor(data) {
    Object.assign(this, data);
  }
}
