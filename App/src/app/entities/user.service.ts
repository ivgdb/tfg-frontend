import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { User } from './user.model';
import { BaseEntityService, EntityAPI, State } from './base-entity.service';
import { Observable, of, throwError } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseEntityService<User> {

  constructor(http: HttpClient, private authService: AuthService) {
    super(User, http);
  }

  getAPI(): EntityAPI {
    return {
      path: '/users/'
    };
  }

  getInitialState(): State<User> {
    return {
      entities: []
    };
  }

  updateUser(data) {
    const request = this.postRequest(this.getAPI().path + 'update', data).pipe(shareReplay());
    return request;
  }
}
