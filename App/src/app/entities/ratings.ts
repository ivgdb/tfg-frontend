import { Entity } from "./base-entity.model";

export class Rating extends Entity {
  constructor(rating) {
    super(rating);
  }
}



import { Injectable } from '@angular/core';
import { BaseEntityService, EntityAPI, State } from './base-entity.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RatingsService extends BaseEntityService<Rating> {

  constructor(http: HttpClient, private authService: AuthService) {
    super(Rating, http);
  }

  getAPI(): EntityAPI {
    return {
      path: '/ratings/'
    };
  }

  getInitialState(): State<Rating> {
    return {
      entities: []
    };
  }

  addRating(game, rating) {
    const request = this.postRequest(this.getAPI().path, { 'videogame': game.uuid, 'rating': rating })
    request.subscribe(ratings => this.dispatch({ entities: this.createList(ratings) }));
    return request;
  }

  getUserRatings(filter) {
    const request = this.getRequest(this.getAPI().path, { 'order': filter });
    request.subscribe(ratings => this.dispatch({ entities: this.createList(ratings) }));
    return request;
  }

  getVideogameRatingByUser(gameUuid?: string) {
    const request = this.getRequest(this.getAPI().path + gameUuid);
    request.subscribe(ratings => this.dispatch({ entities: this.createList(ratings) }));
    return request;
  }
}
