import { Entity } from "./base-entity.model";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { shareReplay, pluck, filter, map } from "rxjs/operators";

export interface State<T extends Entity> {
  entities: T[];
}

export interface EntityAPI {
  path: string;
}

export abstract class BaseEntityService<T extends Entity> {
  private api: EntityAPI;
  private type: new (data) => T;
  private _http: HttpClient;

  protected subject$ = new BehaviorSubject<State<T>>(this.getInitialState());

  protected state$: Observable<State<T>> = this.subject$.asObservable();
  protected entities$: Observable<T[]> = this.state$.pipe(pluck("entities"));

  selectByUuid(uuid): Observable<T> {
    return this.state$.pipe(
      pluck("entities"),
      map(entities => Entity.findById(entities, uuid)),
      filter(e => Boolean(e))
    );
  }

  selectAll(): Observable<T[]> {
    return this.state$.pipe(
      pluck<State<T>, T[]>("entities"),
      filter(e => Boolean(e))
    );
  }

  protected abstract getInitialState(): State<T>;
  protected abstract getAPI(): EntityAPI;

  constructor(type: new (data) => T, http: HttpClient) {
    this.api = this.getAPI();
    this.type = type;
    this._http = http;
  }

  get state() {
    return this.subject$.getValue();
  }

  create(props) {
    const request = this.postRequest(this.api.path, props);

    request.subscribe(entity =>
      this.dispatch({ entities: this.upsert(entity) })
    );

    return request;
  }

  update(entityToUpdate: T) {
    const request = this.postRequest(
      this.api.path + entityToUpdate.id,
      entityToUpdate
    );

    request.subscribe(entity =>
      this.dispatch({ entities: this.upsert(entity) })
    );

    return request;
  }

  delete(entityToDelete: T) {
    const request = this.deleteRequest(this.api.path + entityToDelete.id);

    request.subscribe(() =>
      this.dispatch({
        entities: this.state.entities.filter(
          entity => entity.id !== entityToDelete.id
        )
      })
    );

    return request;
  }

  getByUuid(uuid) {
    const request = this.getRequest(this.api.path + uuid);

    request.subscribe(entity =>
      this.dispatch({ entities: this.upsert(entity) })
    );

    return request;
  }

  getPage({ page, itemsPerPage, ...params }) {
    const isFirstPage = page === 1;
    const request = this.getRequest(this.api.path, {
      page: page.toString(),
      itemsPerPage,
      ...params
    });

    request.subscribe(receivedEntities => {
      const entities = isFirstPage
        ? this.createList(receivedEntities)
        : this.upsertList(receivedEntities);

      this.dispatch({ entities });
    });

    return request;
  }

  getAll(params = {}) {
    const request = this.getRequest(this.api.path, params);

    request.subscribe(entities => {
      this.dispatch({ entities: this.upsertList(entities) });
    });

    return request;
  }

  protected upsert(newEntity) {
    const entity = this.createModel(newEntity);

    const currentEntities = [...this.state.entities];

    this._upsert(currentEntities, entity);

    return currentEntities;
  }

  protected upsertList(newEntities) {
    const entities = this.createList(newEntities);
    const currentEntities = [...this.state.entities];

    entities.forEach(newEntity => {
      this._upsert(currentEntities, newEntity);
    });

    return currentEntities;
  }

  private _upsert(currentEntities: T[], entity: T) {
    const entityIndex = currentEntities.findIndex(
      currentEntity => currentEntity.id === entity.id
    );
    if (entityIndex >= 0) {
      currentEntities[entityIndex] = entity;
    } else {
      currentEntities.push(entity);
    }
  }

  protected createModel(entity): T {
    return new this.type(entity);
  }

  protected createList(entities): T[] {
    return entities.map(entity => this.createModel(entity));
  }

  track(index, entity: T): number {
    return entity.id;
  }

  protected dispatch(state = {}) {
    this.subject$.next({ ...this.state, ...state });
  }

  protected getRequest(url, params = {}): Observable<T | T[]> {
    return this._http.get<T | T[]>(url, { params }).pipe(shareReplay());
  }

  protected postRequest(url, body = {}): Observable<T | T[]> {
    return this._http.post<T | T[]>(url, body).pipe(shareReplay());
  }

  protected putRequest(url, body = {}): Observable<T | T[]> {
    return this._http.put<T | T[]>(url, body).pipe(shareReplay());
  }

  protected deleteRequest(url): Observable<{ success: boolean }> {
    return this._http.delete<{ success: boolean }>(url).pipe(shareReplay());
  }
}
