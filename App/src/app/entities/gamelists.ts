import { Entity } from "./base-entity.model";

export class GameList extends Entity {
  constructor(gamelist) {
    super(gamelist);
  }
}



import { Injectable } from '@angular/core';
import { BaseEntityService, State, EntityAPI } from './base-entity.service';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GamelistsService extends BaseEntityService<GameList>{

  constructor(http: HttpClient, private authService: AuthService) {
    super(GameList, http);
  }

  getAPI(): EntityAPI {
    return {
      path: '/gamelists/'
    };
  }

  getInitialState(): State<GameList> {
    return {
      entities: []
    };
  }

  getByUser() {
    const request = this.getRequest(this.getAPI().path);
    request.subscribe(gamelists => this.dispatch({ entities: this.createList(gamelists) }));
    return request;
  }

  checkGamePlaylist(gameUuid, playlistUuid) {
    const request = this.getRequest(this.getAPI().path + 'exists/videogame/'+gameUuid);
    return request;
  }

  createPlaylist(name, videogameUuid) {
    const request = this.postRequest(this.getAPI().path, { 'name': name, 'videogame': videogameUuid })
    request.subscribe(gamelists => this.dispatch({ entities: this.createList(gamelists) }));
    return request;
  }

  addGameToPlaylist(gameUuid, playlistUuid) {
    const request = this.postRequest(this.getAPI().path + 'add', { 'videogame': gameUuid, 'gamelist': playlistUuid })
    request.subscribe(gamelists => this.dispatch({ entities: this.createList(gamelists) }));
    return request;
  }
}
