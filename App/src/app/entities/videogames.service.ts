import { BaseEntityService, EntityAPI, State } from './base-entity.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { pluck, map, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Entity } from './base-entity.model';
import { pipe } from '@angular/core/src/render3';
import { forkJoin } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Videogame } from 'src/app/entities/videogames.model';

@Injectable({
  providedIn: 'root',
})

export class VideogamesService extends BaseEntityService<Videogame> {

  constructor(http: HttpClient, private authService: AuthService) {
    super(Videogame, http);
  }

  getAPI(): EntityAPI {
    return {
      path: '/games/'
    };
  }

  getInitialState(): State<Videogame> {
    return {
      entities: []
    };
  }

  getByName(query: string) {
    const request = this.getRequest(this.getAPI().path + '?query=' + query);
    request.subscribe(games => this.dispatch({ entities: this.createList(games) }));
    return request;
  }

  selectByFranchise(franchise: string): Observable<Videogame[]> {
    return this.state$.pipe(
      pluck('entities'),
      map((entities: Videogame[]) => entities.filter(entity => entity.franchise == franchise))
    );
  }

  getLatestGames() {
    const request = this.getRequest(this.getAPI().path + '?query=latest');
    request.subscribe(games => this.dispatch({ entities: this.createList(games) }));
    return request;
  }

}
