import { Entity } from "./base-entity.model";

export class Comment extends Entity {
  constructor(comment) {
    super(comment);
  }
}



import { Injectable } from '@angular/core';
import { BaseEntityService, State, EntityAPI } from './base-entity.service';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommentsService extends BaseEntityService<Comment>{

  constructor(http: HttpClient, private authService: AuthService) {
    super(Comment, http);
  }

  getAPI(): EntityAPI {
    return {
      path: '/comments/'
    };
  }

  getInitialState(): State<Comment> {
    return {
      entities: []
    };
  }

  getByUser() {
    const request = this.getRequest(this.getAPI().path);
    request.subscribe(comments => this.dispatch({ entities: this.createList(comments) }));
    return request;
  }

  getByVideogame(videogameUuid) {
    const request = this.getRequest(this.getAPI().path+videogameUuid);
    request.subscribe(comments => this.dispatch({ entities: this.createList(comments) }));
    return request;
  }

  getUserCommentsByVideogame(videogameUuid) {
    const request = this.getRequest(this.getAPI().path+'user/videogame',{'videogame': videogameUuid});
    request.subscribe(comments => this.dispatch({ entities: this.createList(comments) }));
    return request;
  }

  addComment(comment, videogameUuid) {
    const request = this.postRequest(this.getAPI().path, { 'comment': comment, 'videogame': videogameUuid })
    return request;
  }
}
