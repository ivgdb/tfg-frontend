export const environment = {
	production: true,
	APP_URLS: {
		API: 'http://142.93.110.130/api'
	},
	INTERFACE: {
		APP_DEFAULT_LANGUAGE: 'en'
	}
};